import { useRouter } from 'next/dist/client/router';
import Link from 'next/link';
import Image from 'next/image';
import { useEffect, useState } from 'react';
import { Button, Nav, Navbar, NavDropdown } from 'react-bootstrap';
import style from './navigation.module.css';
import { useCookies } from 'react-cookie';


export default function Navigation() {
    const [cookie, setCookie] = useCookies(['jwt']);
    const router = useRouter();
    const [name, setName] = useState<string>();
    const navDropDownTitle = (<Image alt="account_icon" src="/accountIcon.png" width="40px" height="40px"/>);

    async function loadCurrentUser() {
        if (cookie.jwt) {
            // const req = await fetch(`${process.env.NEXT_PUBLIC_HOST_DEV}/User/profile`, {
                const req = await fetch(`${process.env.NEXT_PUBLIC_HOST_PROD}/User/profile`, {
                method: 'GET',
                headers: new Headers({
                    'Authorization': 'Bearer '+ cookie.jwt
                })
            });
            console.log(cookie);
            const user = await req.json();
            console.log('user', user);
            if (user.statusCode == 401) { setCookie('jwt', ''); return null; }
            setName(user.mail);
        }
    }

    function signOut(event: any)  {
        event?.preventDefault();
        cookie.remove('jwt');
    }

    const activeLink = { color: "black"};

    useEffect(() => {
        loadCurrentUser();
    }, [cookie]);

    return (
        <Navbar expand="lg">
            <Navbar.Brand href="/">
                <Image
                    src="/headphoneLogo.jpg"
                    width="50"
                    height="50"
                    className="d-inline-block align-top"
                    alt="headphone logo"
                    
                />
            </Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="mr-auto">
                    <Link href="/">
                        <a className={router.pathname == "/" ? `${style.activeLink} ${style.navlink} ${style.first_navlink}` : `${style.navlink} ${style.first_navlink}`}>Accueil</a>
                    </Link>
                    <Link href="/albums">
                        <a className={router.pathname == "/albums" ? `${style.activeLink} ${style.navlink} ${style.first_navlink}` : `${style.navlink} ${style.first_navlink}`}>Album</a>
                    </Link>
                    <Link href="/songs">
                        <a className={router.pathname == "/songs" ? `${style.activeLink} ${style.navlink} ${style.first_navlink}` : `${style.navlink} ${style.first_navlink}`}>Musique</a>
                    </Link>
                    <Link href="/artists">
                        <a className={router.pathname == "/artists" ? `${style.activeLink} ${style.navlink} ${style.first_navlink}` : `${style.navlink} ${style.first_navlink}`}>Artiste</a>
                    </Link>
                    {
                        cookie.jwt ? 
                        <Link href="/playlist">
                            <a className={router.pathname == "/playlist" ? `${style.activeLink} ${style.navlink} ${style.first_navlink}` : `${style.navlink} ${style.first_navlink}`}>Playlist</a>
                        </Link> : <div></div>
                    }
                </Nav>
            </Navbar.Collapse>
            <div>
                {
                    cookie.name ? <p>Bonjour {name}</p>: ''
                }
            </div>
            <NavDropdown style={{marginRight: "5%"}} title={ navDropDownTitle } id="basic-nav-dropdown">
                {
                    cookie.jwt ?                 
                        // <NavDropdown.Item onClick={ signOut } href="/">Déconnexion</NavDropdown.Item> 
                        <div></div>:
                        <div></div>
                }
                {
                    cookie.jwt ? 
                        <NavDropdown.Item href="/users/profile">Mon Compte</NavDropdown.Item> :
                        <NavDropdown.Item href="/users/login">Connexion</NavDropdown.Item>
                }
                {
                    !cookie.jwt ? 
                        <NavDropdown.Item href="/users/register">Inscription</NavDropdown.Item> : 
                        <div></div>
                }
            </NavDropdown>
            <hr/>
        </Navbar>
    );
}