import Link from "next/link";
import React from "react";
import { ListGroup } from "react-bootstrap";
import { Playlist } from "../../pages/api/dto/playlist.model";

type ListProps = {
    playlist?: Playlist;
}

export default function ListPlaylistTemplate(props: ListProps) {
    return (
        <ListGroup>
            <ListGroup.Item>
                <Link href={ "/playlist/"+props.playlist?.id }>
                    { props.playlist?.name }
                </Link>
            </ListGroup.Item>
        </ListGroup>
    );
}