import React, { Fragment } from 'react';
import { Button, ListGroup } from 'react-bootstrap';
import Card from 'react-bootstrap/Card';
import Link from 'next/link';
import { Song } from '../../pages/api/dto/song.model';
import { Playlist } from '../../pages/api/dto/playlist.model';
import { useState } from 'react';

type CardProps = {
    song?: Song;
    playlist?: Playlist[];
};

export default function CardSongTemplate(props: CardProps) {
    const [open, setOpen] = useState(false);

    function displayPlaylist() {
        console.log('prpos', props.playlist);
        setOpen(true);
    }

    return(
        <div>
            <Card style={{width: "200px", marginTop: "5%"}}>
                <Card.Body>
                {
                    !open ?
                        <Fragment>
                        <Button onClick={ displayPlaylist }>
                            Ajouter à la playlist
                        </Button>
                        <Card.Title>{ props.song?.title }</Card.Title>
                        <Card.Subtitle>{ props.song?.duration } min</Card.Subtitle>
                        <Link href={ "/songs/"+props.song?.id }>
                            <Button variant="primary" >Voir cette musique</Button>
                        </Link>
                        </Fragment> :
                        <ListGroup>
                            {
                                props.playlist?.length === 0 ? 
                                    <div></div> : 
                                    props.playlist?.map((elt, i: number) => 
                                        <ListGroup.Item>
                                            <Link href="#">
                                                { elt?.name }
                                            </Link>
                                        </ListGroup.Item>
                                    )
                            }
                        </ListGroup>
                        
                }
                </Card.Body>
                
            </Card>

        </div>
        
    );
}













