import { Button, Form } from 'react-bootstrap';
import { User } from './../../api/dto/user.model';
import { useState } from 'react';
import styles from '../../../styles/Home.module.css';
import Link from 'next/link';
import { useRouter } from 'next/dist/client/router';
import { Playlist } from '../../api/dto/playlist.model';

export default function register() { 
    const router = useRouter();
    const [mail, setMail] = useState('');
    const [password, setPassword] = useState('');
    const [mailError, setMailError] = useState('');
    const [passwordError, setPasswordError] = useState('');
    const [users, setUsers] = useState<User[]>([]);

    async function onSubmit(event: React.MouseEvent<HTMLButtonElement, MouseEvent>) {
        event?.preventDefault();

        setMailError('');
        setPasswordError('');

        let valid = true; 

        if (mail.trim() === '') {
            setMailError('Vous devez renseigner un titre valide');
            valid = false; 
        }

        if (password.trim() === '') {
            setPasswordError('Vous devez renseigner une année valide');
            valid = false;
        }

        if (valid) {
            const tmpUsers = [...users];
            tmpUsers.push({
                id: 0,
                mail,
                password,
                playlist: []
            });
            setUsers(tmpUsers);
            await getServerSide(mail, password, []);
            setMail('');
            setPassword('');
            router.push('/');
        }
    }

    function onMailChanged(event: React.ChangeEvent<HTMLInputElement>) {
        setMail(event.target.value);
    }

    function onPasswordChanged(event: React.ChangeEvent<HTMLInputElement>) {
        setPassword(event.target.value);
    }

    return (
        <div className="container-user">
            <h1 className={styles.title}>Inscription</h1>
            <Form className="form">
                <Form.Label style={{ marginLeft: "40%" }}>Email:</Form.Label>
                <br/>
                <input style={{ marginLeft: "40%" }} placeholder="mail" type="email" value={mail} onChange={ onMailChanged } />
                <p style={{ marginLeft: "40%" }}>{ mailError }</p>
                <Form.Label style={{ marginLeft: "40%" }}>Mot de passe:</Form.Label>
                <br/>
                <input style={{ marginLeft: "40%" }} placeholder="password" type="password" value={password} onChange={ onPasswordChanged } />
                <p style={{ marginLeft: "40%" }}>{ passwordError }</p>
                
                <Button style={{ marginLeft: "40%" }} type="submit" onClick={onSubmit}>S'inscrire</Button>
            </Form>
            <br/>
            <p style={{ marginLeft: "35%" }}>
                <span>Vous avez déjà un compte ? </span>
                <Link href="/users/login">
                    <a className={router.pathname == "/users/login" ? `${styles.activeLink} ${styles.navlink} ${styles.first_navlink}` : `${styles.navlink} ${styles.first_navlink}`}>Connexion</a>
                </Link>
            </p>
        </div>
    );
}

export async function getServerSide(mail: string, password: string, playlist: Playlist[]) {
    const postBody = {
        mail: mail,
        password: password,
        playlist: playlist
    };
    const requestMetadata = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(postBody)
    };
    await fetch(`${process.env.NEXT_PUBLIC_HOST_PROD}/user/signin`, requestMetadata)
    .then(res => {
        res.json();
        console.log(res)
    })
    .then(recipes => {
        console.log(recipes);
        return ({ recipes });
    });
}