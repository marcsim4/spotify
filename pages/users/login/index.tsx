import React, { useState } from 'react';
import { Button, Form } from 'react-bootstrap';
import Link from 'next/link';
import { useRouter } from 'next/dist/client/router';
import styles from '../../../styles/Home.module.css';

import { Cookies, useCookies } from "react-cookie";
// import { Cookies } from 'js-cookie';

export default function login() {
    const [cookie, setCookie] = useCookies(["jwt"]);
    const router = useRouter();
    const [name, setName] = useState<string>();
    const [mail, setMail] = useState('');
    const [password, setPassword] = useState('');
    const [mailError, setMailError] = useState('');
    const [passwordError, setPasswordError] = useState('');
    const [goToHome, setGoToHome] = useState(false);

    async function onSubmit(event: React.MouseEvent<HTMLButtonElement, MouseEvent>) {
        event?.preventDefault();

        setMailError('');
        setPasswordError('');

        let valid = true; 

        if (mail.trim() === '') {
            setMailError('Vous devez renseigner un titre valide');
            valid = false; 
        }

        if (password.trim() === '') {
            setPasswordError('Vous devez renseigner une année valide');
            valid = false;
        }

        if (valid) {

            const postBody = {
                mail: mail,
                password: password
            };
            const requestMetadata = {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(postBody)
            };
        
            const res = await fetch(`${process.env.NEXT_PUBLIC_HOST_PROD}/User/login`, requestMetadata)
            console.log(res);
            const result: any = await res.json();
            if (result.accessToken) {
                /* removeCookie('jwt',{
                    path: "/",
                    maxAge: 3600, // Expires 1h
                    sameSite: true,
                });*/
                setCookie('jwt', result.accessToken, {
                    path: "/",
                    maxAge: 3600, // Expires 1h
                    sameSite: true,
                });
                console.log('cookie', cookie);
                setMail('');
                setPassword('');
                router.push('/');
            }
        } else {
            console.log('erreur de login');
        }
    }

    function onMailChanged(event: React.ChangeEvent<HTMLInputElement>) {
        setMail(event.target.value);
    }

    function onPasswordChanged(event: React.ChangeEvent<HTMLInputElement>) {
        setPassword(event.target.value);
    }
    return (
        <div className="container-user">
            <main>
                <h1 className={styles.title}>S'identifier</h1>
                <Form className="form">
                    <Form.Label style={{ marginLeft: "40%" }}>Email:</Form.Label>
                    <br/>
                    <input style={{ marginLeft: "40%" }} placeholder="mail" type="email" value={mail} onChange={ onMailChanged } />
                    <p style={{ marginLeft: "40%" }}>{ mailError }</p>
                    <Form.Label style={{ marginLeft: "40%" }}>Mot de passe:</Form.Label>
                    <br/>
                    <input style={{ marginLeft: "40%" }} placeholder="password" type="password" value={password} onChange={ onPasswordChanged } />
                    <p style={{ marginLeft: "40%" }}>{ passwordError }</p>
                    <Button style={{ marginLeft: "40%" }} type="submit" onClick={onSubmit}>S'identifier</Button>
                </Form>
                <br/>
                <p style={{ marginLeft: "35%" }}>
                    <span>Vous n'avez pas encore de compte ? </span>
                    <Link href="/users/register">
                        <a className={router.pathname == "/users/register" ? `${styles.activeLink} ${styles.navlink} ${styles.first_navlink}` : `${styles.navlink} ${styles.first_navlink}`}>Inscription</a>
                    </Link>
                </p>
            </main>
        </div>
    );
}