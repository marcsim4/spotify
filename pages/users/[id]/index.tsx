import { useEffect, useState } from "react";
import { useCookies } from "react-cookie";
import { User } from "../../api/dto/user.model";

export default function user() {
    const [cookie, setCookie] = useCookies(['jwt']);
    const [userName, setUserName] = useState<string>('');
    async function loadCurrentUser() {
        if (cookie.jwt) {
            const req = await fetch(`${process.env.NEXT_PUBLIC_HOST_PROD}/User/profile`, {
                method: 'GET',
                headers: new Headers({
                    'Authorization': 'Bearer '+ cookie.jwt
                })
            });
    
            const user = await req.json();
            console.log('user', user);
            if (user.statusCode == 401) { setCookie('jwt', ''); return null; }
            setUserName(user.mail);
        }   
    }

    useEffect(() => {
        loadCurrentUser();
    }, [cookie]);

    return (
        <main>
            <h1>Bonjour {userName}</h1>
            
        </main>
    );
}