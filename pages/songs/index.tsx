
import Link from 'next/link';
import { Song } from "../api/dto/song.model";
import styles from '../../styles/Home.module.css'
import React, { useEffect, useState } from 'react';
import { Button } from 'react-bootstrap';
import CardSongTemplate from '../../component/card/cardSong.component';
import { useCookies } from 'react-cookie';
import { Playlist } from '../api/dto/playlist.model';

type ISongProps = {
    songs: Song[];
    playlists: Playlist[];
}

export default function song(props: ISongProps) {    
    const [cookie, useCookie] = useCookies(['jwt']);
    return (
        <div>
            <div className="container">
                <h1 className={styles.title}>Musique</h1>
                <div className={styles.btn_add_right}>
                    {
                        cookie.jwt ?  
                        <div className="btn-add">
                            <Link href="/songs/addSong">
                                <Button variant="danger">Ajouter une musique</Button>
                            </Link>
                        </div> : <div></div>
                    }
                </div>
                <div className={styles.cardMargin}>    
                {
                    props.songs.length === 0 ? <p>Aucune musique disponible</p> :
                    <div className="row">
                    {
                        props.songs.map((song, i) => 
                            <div className="col-3">
                                <CardSongTemplate key={i} song={ song } playlist={ props.playlists } />
                            </div>
                        )
                    }
                    </div>
                }
                </div>
            </div>
        </div>
    );
}

export async function getStaticProps(context: any) {
    const res = await fetch(`${process.env.NEXT_PUBLIC_HOST_PROD}/song`)
    // const res = await fetch(`${process.env.NEXT_PUBLIC_HOST_DEV}/song`)
    const songs : Song[] = await res.json()

    const res2 = await fetch(`${process.env.NEXT_PUBLIC_HOST_PROD}/playlist`)
    const playlists : Playlist[] = await res2.json()
    console.log(songs);
    console.log('playlist', playlists);
    if (!songs) {
        return {
            redirect: {
                destination: '/',
                permanent: false,
            },
        }
    }
    return {
        props: { songs, playlists }, // will be passed to the page component as props
        revalidate : 15
    }
}