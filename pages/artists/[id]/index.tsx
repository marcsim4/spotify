import { Artist } from "../../api/dto/artist.model";
import Link from 'next/link';
import React, { useState } from "react";
import { useRouter } from 'next/router';
import { Button } from "react-bootstrap";
import styles from '../../../styles/Home.module.css'
import { Album } from "../../api/dto/album.model";
import CardAlbumTemplate from "../../../component/card/cardAlbum.component";


type IArtistProps  = {
    artists: Artist;
    albums: Album[];
}

export default function ArtistItem(props: IArtistProps) {
    const [album, setAlbum] = useState([]);
    const router = useRouter();

    async function onDelete() {
        if (props.artists) {
            await getServerSide(props.artists);
            router.push('/artists')
        } else {
            console.log('error');
        }
    }

    return (
        <div>
            <main className={styles.cardMargin}>
                <Link href="/artists"> 
                    <Button className={styles.btn_add_right} variant="dark">Retour</Button>
                </Link>
                <h1 className={styles.title}>{props.artists.name}</h1>
                <p style={{ marginLeft: "45%"}}>{props.artists.isBand}</p>
                <Button style={{ marginLeft: "40%"}} onClick={ onDelete }>Supprimer l'artiste</Button>
            </main>

            <div>
                <div className={styles.cardMargin}>
                    {
                        props.albums && props.albums?.length === 0 ? <p>Aucun album disponible</p> :     
                        <div className="row">
                        {
                            props.albums?.map((elt, i) => 
                                <div className="col-3">
                                    <CardAlbumTemplate key={i} album={ elt } />
                                </div>
                            )
                        }
                        </div>
                    }
                </div>
            </div>
        </div>
    );
}

export async function getServerSideProps(context: any) {
    const res = await fetch(`${process.env.NEXT_PUBLIC_HOST_PROD}/artist/${context.query.id}`)
    // const res = await fetch(`${process.env.NEXT_PUBLIC_HOST_DEV}/artist/${context.query.id}`)
    const artists : Artist = await res.json()
    let albumList: Album[] = [];
    console.log('artist', artists);
    if (!artists) {
        return {
            redirect: {
                destination: '/',
                permanent: false,
            },
        }
    } else {
        if (artists.albums && artists.albums?.length > 0) {
            for (const $item of artists.albums) {
                 albumList.push($item);
             }
         }
    }
    return {
        props: { artists: artists, albums: albumList }
    }
}

export async function getServerSide(artist: Artist) {
    const requestOptions = {
        method: 'delete'
    };
    await fetch(`${process.env.NEXT_PUBLIC_HOST_PROD}/Artist/${artist.id}`, requestOptions)
    // await fetch(`${process.env.NEXT_PUBLIC_HOST_DEV}/Artist/${artist.id}`, requestOptions)
    .then(res =>res.json())
    .then(recipes => {
        console.log(recipes);
        return ({ recipes });
    });
}