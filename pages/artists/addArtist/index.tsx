import React, { useEffect, useState } from "react";
import { useRouter } from 'next/router';
import Link from 'next/link';
import { Button, Form } from "react-bootstrap";
import { Artist } from "../../api/dto/artist.model";
import styles from '../../../styles/Home.module.css'
import { Album } from "../../api/dto/album.model";

type IAlbumProps = {
    albums: Album[];
}

export default function addArtist(props: IAlbumProps) {
    const [name, setName] = useState('');
    const [isBand, setIsBand] = useState(false);
    const [albums, setAlbum] = useState<Album[]>([]);
    const [nameError, setNameError] = useState('');
    const [isBandError, setIsBandError] = useState('');
    const [artists, setArtists] = useState<Artist[]>([]);
    const router = useRouter();


    async function onSubmit(event: React.MouseEvent<HTMLButtonElement, MouseEvent>) {
        event?.preventDefault();

        setNameError('');
        setIsBandError('');

        let valid = true; 

        if (name.trim() === '') {
            setNameError('Vous devez renseigner un nom valide');
            valid = false; 
        }

        if (isBand === undefined) {
            setIsBandError('Vous devez renseigner si l\'artiste fait parti d\'une bande.');
            valid = false;
        }

        // if (album.length === 0) {
        //     const tmpAlbum = [...album];
        //     tmpAlbum.push(0);
        //     setAlbum(tmpAlbum);
        // }

        if (valid) {
            const tmpArtist = [...artists];
            tmpArtist.push({
                id: 0,
                name,
                isBand,
                albums
            });
            setArtists(tmpArtist);
            await getServerSide(name, isBand, albums);
            router.push('/artists');
        }
    }

    function onNameChanged(event: React.ChangeEvent<HTMLInputElement>) {
        setName(event.target.value);
    }

    function onIsBandChanged(event: React.ChangeEvent<HTMLInputElement>) {
        console.log('event', event);
    }

    async function onAlbumChanged(event: React.ChangeEvent<HTMLSelectElement>) {
        // const tmpAlbum = [...album];
        // tmpAlbum.push(+event.target.value);
        // setAlbum(tmpAlbum);
    }

    return (
        <div className="container-user">
            <h1 className={styles.title}>Ajouter un artiste</h1>
            <Link href="/artists"> 
                <Button className={styles.btn_add_right} variant="dark">Retour</Button>
            </Link>
            <Form className={styles.cardMargin}>
                <Form.Label style={{ marginLeft: "40%" }}>Nom :</Form.Label>
                <br/>
                <input style={{ marginLeft: "40%" }} placeholder="Nom" type="text" value={name} onChange={ onNameChanged } />
                <p style={{ marginLeft: "40%" }}>{ nameError }</p>
                <Form.Label style={{ marginLeft: "40%" }}>L'artiste est-il en bande ? </Form.Label>
                <input style={{ marginLeft: "2%" }}type="checkbox" onChange={ onIsBandChanged } />
                <p style={{ marginLeft: "40%" }}>{ isBandError }</p>
                <Form.Label style={{ marginLeft: "40%" }}>Albums : </Form.Label>
                <select style={{ marginLeft: "2%" }} onChange={ onAlbumChanged }>
                {/* multiple */}
                    <option value="0">-------</option>
                    {
                        props.albums.map((item) => (
                            <option value={item.id}>{item.title}</option>
                        ))
                    }
                </select>
                <br/>
                <Button style={{ marginLeft: "40%" }} type="submit" onClick={onSubmit}>Valider</Button>
            </Form>
        </div>
    );
}

export async function getServerSide(name: string, isBand: boolean, album?: Album[]) {
    const postBody = {
        name: name,
        isBand: isBand,
        album: album
    };
    const requestMetadata = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(postBody)
    };
    await fetch(`${process.env.NEXT_PUBLIC_HOST_PROD}/artist/addArtist`, requestMetadata)
    // await fetch(`${process.env.NEXT_PUBLIC_HOST_DEV}/artist/addArtist`, requestMetadata)

    .then(res =>res.json())
    .then(recipes => {
        console.log(recipes);
        return ({ recipes });
    });
}

export async function getStaticProps(context: any) {
    const res = await fetch(`${process.env.NEXT_PUBLIC_HOST_PROD}/album`)
    // const res = await fetch(`${process.env.NEXT_PUBLIC_HOST_DEV}/album`)
    const albums : Album[] = await res.json()
    if (!albums) {
        console.log('List empty');
    }
    console.log('album', albums);
    return {
        props: { albums }, // will be passed to the page component as props
        revalidate : 15
    }
}