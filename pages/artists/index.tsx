
import Link from 'next/link';
import { Artist } from "../api/dto/artist.model";
import styles from '../../styles/Home.module.css'
import { Button } from 'react-bootstrap';
import CardArtistTemplate from '../../component/card/cardArtist.component';
import { useCookies } from 'react-cookie';

type IArtistProps = {
    artists: Artist[];
}

export default function artist(props: IArtistProps) {    
    const [cookie, setCookie] = useCookies(['jwt']);
    return (
        <div>
            <div className="container">
                <h1 className={styles.title}>Artiste</h1>
                <div className={styles.btn_add_right}>
                    {
                        cookie.jwt ?  
                        <div className="btn-add">
                            <Link href="/artists/addArtist">
                                <Button variant="danger">Ajouter un artiste</Button>
                            </Link>
                        </div> : <div></div>
                    }
                </div>
                <div className={styles.cardMargin}>
                    {
                        props.artists.length === 0 ? <p>Aucun artiste disponible</p> :     
                        <div className="row">
                        {
                            props.artists.map((artist, i) => 
                                <div className="col-3">
                                    <CardArtistTemplate key={i} artist={ artist } />
                                </div>
                            )
                        }
                        </div>
                    }
                </div>
            </div>
        </div>
    );
}

export async function getStaticProps(context: any) {
    const res = await fetch(`${process.env.NEXT_PUBLIC_HOST_PROD}/artist`)
    // const res = await fetch(`${process.env.NEXT_PUBLIC_HOST_DEV}/artist`)
    const artists : Artist[] = await res.json()
    if (!artists) {
        return {
            redirect: {
                destination: '/',
                permanent: false,
            },
        }
    }
    console.log(artists);
    return {
        props: { artists }, // will be passed to the page component as props
        revalidate : 15
    }
}