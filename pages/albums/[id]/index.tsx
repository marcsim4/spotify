import { useRouter } from 'next/router';
import Link from 'next/link';
import React, { useEffect, useState } from 'react';
import { Button } from 'react-bootstrap';
import { Album } from '../../api/dto/album.model';
import styles from '../../../styles/Home.module.css'
import CardArtistTemplate from '../../../component/card/cardArtist.component';
import { Artist } from '../../api/dto/artist.model';
import { Song } from '../../api/dto/song.model';
import CardSongTemplate from '../../../component/card/cardSong.component';


type IAlbumProps  = {
    albums: Album;
    artist: Artist[];
    song: Song[];
}

export default function AlbumItem(props: IAlbumProps) {
    const [artist, setArtist] = useState([]);
    const [song, setSong] = useState([]);
    const router = useRouter()

    async function onDelete() {
        if (props.albums) {
            await getServerSide(props.albums);
            router.push('/albums')
        } else {
            console.log('error');
        }
    }

    return (
        <div>
            <main className={styles.cardMargin}>
                <Link href="/albums"> 
                    <Button className={styles.btn_add_right} variant="dark">Retour</Button>
                </Link>
                <img style={{ marginLeft: "35%"}} width="150px" height="150px" src={props.albums.cover} alt="couverture de l'album" />
                <h1 className={styles.title}>{props.albums.title}</h1>
                <p style={{ marginLeft: "45%"}}>{props.albums.year}</p>
                <Button style={{ marginLeft: "40%"}} variant="danger" onClick={ onDelete }>Supprimer l'album</Button>
            </main>

            <div>
                <div className={styles.cardMargin}>
                    {
                        props.artist && props.artist?.length === 0 ? <p>Aucun artiste disponible</p> :     
                        <div className="row">
                        {
                            props.artist?.map((elt, i) => 
                                <div className="col-3">
                                    <CardArtistTemplate key={i} artist={ elt } />
                                </div>
                            )
                        }
                        </div>
                    }
                </div>
            </div>
            <div>
                <div className={styles.cardMargin}>
                    {
                        props.song && props.song?.length === 0 ? <p>Aucun musique disponible</p> :     
                        <div className="row">
                        {
                            props.song?.map((elt, i) => 
                                <div className="col-3">
                                    <CardSongTemplate key={i} song={ elt } />
                                </div>
                            )
                        }
                        </div>
                    }
                </div>
            </div>
        </div>
    );
}

export async function getServerSideProps(context: any) {
    const res = await fetch(`${process.env.NEXT_PUBLIC_HOST_PROD}/album/${context.query.id}`)
    // const res = await fetch(`${process.env.NEXT_PUBLIC_HOST_DEV}/album/${context.query.id}`)
    const albums : Album = await res.json()
    let artistList: Artist[] = [];
    let songList: Song[] = [];
    console.log('albums', albums);
    if (!albums) {
        return {
            redirect: {
                destination: '/',
                permanent: false,
            },
        }
    } else {
        if (albums.artists && albums.artists?.length > 0) {
           for (const $item of albums.artists) {
                artistList.push($item);
            }
        }
        if (albums.songs && albums.songs?.length > 0) {
            for (const $item of albums.songs) {
                songList.push($item);
            }
        }
    }
    return {
        props: { albums: albums, artist: artistList, song: songList }
    }
}

export async function getServerSide(album: Album) {
    const requestOptions = {
        method: 'delete'
    };
    await fetch(`${process.env.NEXT_PUBLIC_HOST_PROD}/Album/${album.id}`, requestOptions)
    // await fetch(`localhost:3001/Album/${album.id}`, requestOptions)

    .then(res =>res.json())
    .then(recipes => {
        console.log(recipes);
        return ({ recipes });
    });
}