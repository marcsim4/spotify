import React, { useEffect, useState } from "react";
import { useRouter } from 'next/router';
import Link from 'next/link';
import { Button, Form } from "react-bootstrap";
import { Album } from "../../api/dto/album.model";
import styles from '../../../styles/Home.module.css'

import { useDropzone } from "react-dropzone";
import { Artist } from "../../api/dto/artist.model";
import { Song } from "../../api/dto/song.model";

type IArtistProps  = {
    artists: Artist[];
    songs: Song[];
}

export default function addAlbum(props: IArtistProps) {
    const [loading, setLoading] = useState(true);
    const [file, setFile] = useState([]);
    const [title, setTitle] = useState('');
    const [year, setYear] = useState('');
    const [cover, setCover] = useState('');

    const [artists, setArtist] = useState<Artist[]>([]);
    const [songs, setSong] = useState<Song[]>([]);

    const [titleError, setTitleError] = useState('');
    const [yearError, setYearError] = useState('');
    const [albums, setAlbums] = useState<Album[]>([]);

    const router = useRouter();

    async function onSubmit(event: React.MouseEvent<HTMLButtonElement, MouseEvent>) {
        event?.preventDefault();

        setTitleError('');
        setYearError('');

        let valid = true; 

        if (title.trim() === '') {
            setTitleError('Vous devez renseigner un titre valide');
            valid = false; 
        }

        if (year.trim() === '') {
            setYearError('Vous devez renseigner une année valide');
            valid = false;
        }

        if (cover.trim() === '') {
            setCover('https://cdn2.clc2l.fr/c/thumbnail280/t/a/p/apple-music-tNKCFp.jpg');
        }

        if (valid) {
            const tmpAlbum = [...albums];
            tmpAlbum.push({
                id: 0,
                title,
                year,
                cover,
                artists,
                songs
            });
            setAlbums(tmpAlbum);
            await getServerSide(title, year, cover, artists, songs);
            router.push('/albums')
        }
    }

    function onTitleChanged(event: React.ChangeEvent<HTMLInputElement>) {
        setTitle(event.target.value);
    }

    function onYearChanged(event: React.ChangeEvent<HTMLInputElement>) {
        setYear(event.target.value);
    }

    function onCoverChanged(event: React.ChangeEvent<HTMLInputElement>) {
        setCover(event.target.value);
    }

    async function onArtistChanged(event: React.ChangeEvent<HTMLSelectElement>) {
        const tmpArtist = [...artists];
        for (const $item of props.artists) {
            if ($item.id === +event.target.value) {
                tmpArtist.push($item);
            }
        }
        setArtist(tmpArtist);
    }

    async function onSongChanged(event: React.ChangeEvent<HTMLSelectElement>) {
        console.log('event', event);
        const tmpSong = [...songs];
        for (const $item of props.songs) {
            if ($item.id === +event.target.value) {
                tmpSong.push($item);
            }
        }
        setSong(tmpSong);
    }



    // const images = file.map((file) => (
    //     // <div key={file.name}>
    //     //     <div>
    //     //         <img src={file.preview} style={{width: "200px" }} alt="preview" />
    //     //     </div>
    //     // </div>
    //     <p>{file.preview}</p>
    // ))

    return (
        <div className="container-user">
            <h1 className={styles.title}>Ajouter un album</h1>
            <Link href="/albums"> 
                <Button className={styles.btn_add_right} variant="dark">Retour</Button>
            </Link>
            <Form className={styles.cardMargin}>
                <Form.Label style={{ marginLeft: "40%" }}>Titre :</Form.Label>
                <br/>
                <input style={{ marginLeft: "40%" }} placeholder="Titre" type="text" value={title} onChange={ onTitleChanged } />
                <p style={{ marginLeft: "40%" }}>{ titleError }</p>
                <Form.Label style={{ marginLeft: "40%" }}>Artistes : </Form.Label>
                <select style={{ marginLeft: "2%" }} onChange={ onArtistChanged }>
                {/* multiple */}
                    <option value="0">-------</option>
                    {
                        props.artists.map((item) => (
                            <option value={item.id}>{item.name}</option>
                        ))
                    }
                </select>
                <br/>
                <Form.Label style={{ marginLeft: "40%" }}>Musiques : </Form.Label>
                <select style={{ marginLeft: "2%" }} onChange={ onSongChanged }>
                {/* multiple */}
                    <option value="0">-------</option>
                    {
                        props.songs.map((item) => (
                            <option value={item.id}>{item.title}</option>
                        ))
                    }
                </select>
                <br/>
                <Form.Label style={{ marginLeft: "40%" }}>Année : </Form.Label>
                <br/>                
                <input style={{ marginLeft: "40%" }} placeholder="Année" type="text" value={year} onChange={ onYearChanged } />
                <p style={{ marginLeft: "40%" }}>{ yearError }</p>
                <Form.Label style={{ marginLeft: "40%" }}>Image de couverture : </Form.Label>
                <br/>
                <input style={{ marginLeft: "40%", width: "300px" }} value='https://cdn2.clc2l.fr/c/thumbnail280/t/a/p/apple-music-tNKCFp.jpg' type="text" placeholder="URL de l'image" onChange={ onCoverChanged } />
                {/* <input placeholder="Image" type="file" onChange={ onCoverChanged } /> */}
                {/* <div {...getRootProps()}>
                    <input {...getInputProps()} />
                    <p>Drop file here</p>
                </div> */}
                <div>
                    {
                        cover ? <img src={cover} style={{ marginLeft: "40%", marginBottom: "5%" }} width="150px" height="150px" alt="couverture de l'album" /> : <span></span> 
                    }
                </div>
                <Button style={{ marginLeft: "40%" }} type="submit" onClick={onSubmit}>Valider</Button>
            </Form>
        </div>
    );
}

export async function getServerSide(title?: string, year?: string, cover?: string, artist?: Artist[], song?: Song[]) {
    const postBody = {
        title: title,
        year: year,
        cover: cover,
        artist: artist,
        song: song
    };
    const requestMetadata = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(postBody)
    };
    await fetch(`${process.env.NEXT_PUBLIC_HOST_PROD}/Album/addAlbum`, requestMetadata)
    // await fetch(`${process.env.NEXT_PUBLIC_HOST_DEV}/Album/addAlbum`, requestMetadata)
    .then(res =>res.json())
    .then(recipes => {
        console.log('recipes', recipes);
        return ({ recipes });
    });
}

export async function getStaticProps(context: any) {
    const res = await fetch(`${process.env.NEXT_PUBLIC_HOST_PROD}/artist`)
    // const res = await fetch(`${process.env.NEXT_PUBLIC_HOST_DEV}/artist`)
    const artists : Artist[] = await res.json()
    if (!artists) {
        console.log('List artist empty');
    }
    // const res2 = await fetch(`${process.env.NEXT_PUBLIC_HOST_DEV}/song`)
    const res2 = await fetch(`${process.env.NEXT_PUBLIC_HOST_PROD}/song`)
    const songs: Song[] = await res2.json()
    if (!songs) {
        console.log('List song empty');
    }
    return {
        props: { artists: artists, songs: songs },
        revalidate : 15
    }
}
