import { Album } from "../api/dto/album.model";
import Link from 'next/link';
import { Button } from 'react-bootstrap';
import styles from '../../styles/Home.module.css'
import CardAlbumTemplate from '../../component/card/cardAlbum.component';
import { useCookies } from "react-cookie";

type IAlbumProps  = {
    albums: Album[];
}

export default function album(props: IAlbumProps) {
    const [cookie, setCookie] = useCookies(['jwt']);
    return (
        <div>
            <div className="container">
                <h1 className={styles.title}>Albums</h1>
                <div className={styles.btn_add_right}>
                    {
                        cookie.jwt ?
                        <div className="btn-add">
                            <Link href="/albums/addAlbum">
                                <Button variant="danger">Ajouter un album</Button>
                            </Link>
                        </div> : <div></div>
                    }
                </div>
                <div className={styles.cardMargin}>    
                {
                    props.albums.length === 0 ? <p>Aucun album disponible</p> : 
                    <div className="row">
                    {
                        props.albums.map((album, i: number) => 
                            <div className="col-3">
                                <CardAlbumTemplate key={i} album={ album } />
                            </div>
                        )
                    }
                    </div>
                }
                </div>
            </div>
        </div>
    );
}

export async function getStaticProps(context: any) {

    const res = await fetch(`${process.env.NEXT_PUBLIC_HOST_PROD}/album`)
    // const res = await fetch(`${process.env.NEXT_PUBLIC_HOST_DEV}/album`)
    const albums : Album[] = await res.json()
    if (!albums) {
        return {
            redirect: {
                destination: '/',
                permanent: false,
            },
        }
    }
    return {
        props: { albums },
        revalidate : 15
    }
}