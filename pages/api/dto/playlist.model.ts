import { Song } from "./song.model";
import { User } from "./user.model";

export interface Playlist {
  id: number;
  name: string;
  user?: User;
  songs: Song[];
}