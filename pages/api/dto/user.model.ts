import { Playlist } from "./playlist.model";

export interface User {
    id: number;
    mail: string;
    password: string;
    playlist: Playlist[];
}