import { Artist } from "./artist.model";
import { Song } from "./song.model";

export interface Album {
    id: number;
    title: string;
    year: string; //Todo date format
    cover: string; //Todo voir couverture image
    artists: Artist[];
    songs: Song[];
  }
  