import { Album } from "./album.model";

export interface Artist {
    id: number;
    name: string;
    isBand: boolean;
    albums?: Album[];
    // songList: Song[];
}
  