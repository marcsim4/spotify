import '../styles/globals.css'
import type { AppProps } from 'next/app'
import 'bootstrap/dist/css/bootstrap.min.css';
import styles from '../styles/Home.module.css'
import Navigation from '../component/navigation/navigation.component';
import { CookiesProvider } from 'react-cookie';

function MyApp({ Component, pageProps }: AppProps) {
  
  return (
    <div>
      <Navigation />
      <CookiesProvider>
        <Component {...pageProps} />
      </CookiesProvider>
      <footer className={styles.footer}>
        <a
          href="#"
          target="_blank"
          rel="noopener noreferrer"
        >
          Powered by{' Marc-antoine SIMON - MDS 2021'}
        </a>
      </footer>
    </div>
  )
}
export default MyApp
