import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { Playlist } from "../../api/dto/playlist.model";
import { Song } from "../../api/dto/song.model";
import styles from '../../../styles/Home.module.css'
import { Button, Form } from "react-bootstrap";
import Link from "next/link";
import { useCookies } from "react-cookie";
import { User } from "../../api/dto/user.model";


type IAddPlaylistProps  = {
    songs: Song[];
}

export default function addPlaylist(props: IAddPlaylistProps) {
    const [name, setName] = useState('');
    const [user, setUser] = useState<User>();
    const [nameError, setNameError] = useState('');
    const [songs, setSongs] = useState<Song[]>([]);
    const [playlist, setPlaylist] = useState<Playlist[]>([]);
    const router = useRouter();
    const [cookie, setCookie] = useCookies(['jwt']);


    async function loadCurrentUser() {
        if (cookie.jwt) {
            const req = await fetch(`${process.env.NEXT_PUBLIC_HOST_PROD}/User/profile`, {
                method: 'GET',
                headers: new Headers({
                    'Authorization': 'Bearer '+ cookie.jwt
                })
            });
    
            const user = await req.json();
            console.log('user', user);
            if (user.statusCode == 401) { setCookie('jwt', ''); return null; }
            setUser(user);
        }
    }

    useEffect(() => {
        loadCurrentUser();
    }, [cookie]);

    async function onSubmit(event: React.MouseEvent<HTMLButtonElement, MouseEvent>) {
        event?.preventDefault();

        setNameError('');
        let valid = true; 

        if (name.trim() === '') {
            setNameError('Vous devez entrer un nom pour votre playlist');
            valid = false; 
        }

        if (valid) {
            const tmpPlaylist = [...playlist];
            tmpPlaylist.push({
                id: 0,
                name,
                user,
                songs,
            });
            console.log(tmpPlaylist);
            setPlaylist(tmpPlaylist);
            await getServerSide(name, songs); 
            router.push('/playlist')
        }
    }

    function onNameChanged(event: React.ChangeEvent<HTMLInputElement>) {
        setName(event.target.value);
    }

    return (
        <div className="container-user">
            <h1 className={styles.title}>Ajouter une playlist</h1>
            <Link href="/albums"> 
                <Button className={styles.btn_add_right} variant="dark">Retour</Button>
            </Link>
            <Form className={styles.cardMargin}>
                <Form.Label style={{ marginLeft: "40%" }}>Nom de la playlist :</Form.Label>
                <br/>
                <input style={{ marginLeft: "40%" }} placeholder="Nom" type="text" value={name} onChange={ onNameChanged } />
                <p style={{ marginLeft: "40%" }}>{ nameError }</p>
                <br/>
                {/* <Form.Label style={{ marginLeft: "40%" }}>Musiques : </Form.Label>
                <select style={{ marginLeft: "2%" }} onChange={ onSongChanged }>
                    <option value="0">-------</option>
                    {
                        props.songs.map((item) => (
                            <option value={item.id}>{item.title}</option>
                        ))
                    }
                </select>
                <br/> */}
                <Button style={{ marginLeft: "40%" }} type="submit" onClick={onSubmit}>Valider</Button>
            </Form>
        </div>
    );
}

export async function getServerSide(name?: string, songs?: Song[]) {
    
    const postBody = {
        name: name,
        songs: []
    };
    const requestMetadata = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(postBody)
    };
    await fetch(`${process.env.NEXT_PUBLIC_HOST_PROD}/playlist/addPlaylist`, requestMetadata)
    // await fetch(`${process.env.NEXT_PUBLIC_HOST_DEV}/playlist/addPlaylist`, requestMetadata)
    .then(res =>res.json())
    .then(recipes => {
        console.log('recipes', recipes);
        return ({ recipes });
    });
}
