import { useRouter } from "next/router";
import React, { useState } from "react";
import { Button, ListGroup } from "react-bootstrap";
import { Playlist } from "../../api/dto/playlist.model";
import { Song } from "../../api/dto/song.model";

type IPlaylistProps = {
    playlist: Playlist;
    song: Song[];
}

export default function PlaylistItem(props: IPlaylistProps) {
    const [song, setSong] = useState([]);
    const router = useRouter();
    
    async function onDelete() {
        if (props.playlist) {
            await getServerSide(props.playlist);
            router.push('/playlist')
        } else {
            console.log('error');
        }
    }
    return (
        <div>
            <h1 style={{ marginLeft: "40%"}}>{ props.playlist.name }</h1>
            <Button style={{ marginLeft: "40%"}} variant="danger" onClick={ onDelete }>Supprimer la playlist</Button>
            {
                props.song && props.song?.length === 0 ? <p>Playlist vide</p> :
                <ListGroup>
                    {
                        props.song?.map((elt, i:number) => 
                            <ListGroup.Item>
                                { elt?.title }
                            </ListGroup.Item>
                        )
                    }
                    
                </ListGroup>
            }
        </div>
    );

}

export async function getServerSideProps(context: any) {
    const res = await fetch(`${process.env.NEXT_PUBLIC_HOST_PROD}/album/${context.query.id}`)
    // const res = await fetch(`${process.env.NEXT_PUBLIC_HOST_DEV}/playlist/${context.query.id}`)
    const playlist : Playlist = await res.json()
    let songList: Song[] = [];
    console.log('playlist', playlist);
    if (!playlist) {
        return {
            redirect: {
                destination: '/',
                permanent: false,
            },
        }
    } else {
        if (playlist.songs && playlist.songs?.length > 0) {
            for (const $item of playlist.songs) {
                songList.push($item);
            }
        }
    }
    return {
        props: { playlist: playlist, song: songList }
    }
}

export async function getServerSide(playlist: Playlist) {
    const requestOptions = {
        method: 'delete'
    };
    await fetch(`${process.env.NEXT_PUBLIC_HOST_PROD}/playlist/${playlist.id}`, requestOptions)
    // await fetch(`${process.env.NEXT_PUBLIC_HOST_DEV}/playlist/${playlist.id}`, requestOptions)
    .then(res =>res.json())
    .then(recipes => {
        console.log(recipes);
        return ({ recipes });
    });
}