import { useCookies } from "react-cookie";
import { Playlist } from "../api/dto/playlist.model";
import styles from '../../styles/Home.module.css'
import Link from 'next/link';
import { Button } from 'react-bootstrap';
import React, { useEffect } from "react";
import ListPlaylistTemplate from "../../component/listPlaylist/listPlaylist";

type IPlaylistProps = {
    playlist: Playlist[];
}

export default function playlist(props: IPlaylistProps) {
    const [cookie, setCookie] = useCookies(['jwt']);

    useEffect(() => {
        console.log('cookie', cookie);
    }, [cookie]);

    return (
        <div className="container">
            <h1 className={styles.title}>Mes playlists</h1>
            <div className={styles.btn_add_right}>
                {
                    cookie.jwt ?
                    <div className="btn-add">
                        <Link href="/playlist/addPlaylist">
                            <Button variant="danger">Ajouter une Playlist</Button>
                        </Link>
                    </div> : <div></div>
                }
            </div>
            <div className={styles.cardMargin}>    
            {
                props.playlist?.length === 0 ? <p>Aucune playlist disponible</p> : 
                <div className="row">
                {
                    props.playlist?.map((playlist, i: number) => 
                        <div className="col-8">
                            <ListPlaylistTemplate key={i} playlist={ playlist } />
                        </div>
                    )
                }
                </div>
            }
            </div>
        </div>
    );
}

export async function getStaticProps(context: any) {

    const res = await fetch(`${process.env.NEXT_PUBLIC_HOST_PROD}/playlist`)
    // const res = await fetch(`${process.env.NEXT_PUBLIC_HOST_DEV}/playlist`)
    const playlists : Playlist[] = await res.json()
    if (!playlists) {
        return {
            redirect: {
                destination: '/',
                permanent: false,
            },
        }
    }
    return {
        props: { playlist: playlists },
        revalidate : 15
    }
}
